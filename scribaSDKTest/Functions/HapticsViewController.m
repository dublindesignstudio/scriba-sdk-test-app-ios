//
//  HapticsViewController.m
//  scribaSDKTest
//
//  Created by lei_zhang on 4/21/16.
//  Copyright © 2016 Scriba. All rights reserved.
//

#import "HapticsViewController.h"
#import "UITextView+HTML.h"

@interface HapticsViewController()

@property (nonatomic,weak) IBOutlet UILabel *zone0;
@property (nonatomic,weak) IBOutlet UILabel *zone1;
@property (nonatomic,weak) IBOutlet UILabel *zone2;
@property (nonatomic,weak) IBOutlet UILabel *zone3;
@property (nonatomic,weak) IBOutlet UILabel *zone4;

@property (nonatomic,weak) IBOutlet UILabel *titleLabel;

@property (nonatomic,weak) IBOutlet UITextView *descTextView;

@end

@implementation HapticsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    NSString *descString = NSLocalizedString(@"haptics.desc.text", nil);
    
//    NSAttributedString *attributedString = [[NSAttributedString alloc]
//                                            initWithData: [descString dataUsingEncoding:NSUnicodeStringEncoding]
//                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
//                                            documentAttributes: nil
//                                            error: nil
//                                            ];
    
    
    NSLog(@" self.descTextView.font.fontName %@", self.descTextView.font.fontName);
    
    [self.descTextView setHTMLFromString:descString];
    
//    self.descTextView.attributedText = attributedString;
    
//    [[ScribaStylusManager sharedManager] enableHapticsBuzz];

}

-(void)didChangedDepressionForDevice:(ScribaStylusDevice*)device depression:(float)depression{
    

    self.titleLabel.text = [NSString stringWithFormat:@"Squeeze Zone value : %.2f",depression];
    
}

/*!
 * @discussion Callback method when squeeze zone is changed
 * @param device The scriba device triggers the event
 * @param squeezeZone The squeeze zone level is returned.
 */
-(void)didChangedSqueezeZoneForDevice:(ScribaStylusDevice*)device squeezeZone:(NSInteger)squeezeZone{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hightlightZone:squeezeZone];
    });
}

- (void)hightlightZone:(NSInteger)selectedZone
{
    self.zone0.backgroundColor = [UIColor lightGrayColor];
    self.zone1.backgroundColor = [UIColor lightGrayColor];
    self.zone2.backgroundColor = [UIColor lightGrayColor];
    self.zone3.backgroundColor = [UIColor lightGrayColor];
    self.zone4.backgroundColor = [UIColor lightGrayColor];
    
    switch (selectedZone) {
        case 0:
            self.zone0.backgroundColor = [UIColor blueColor];
            break;
        case 1:
            self.zone1.backgroundColor = [UIColor blueColor];
            break;
        case 2:
            self.zone2.backgroundColor = [UIColor blueColor];
            break;
        case 3:
            self.zone3.backgroundColor = [UIColor blueColor];
            break;
        case 4:
            self.zone4.backgroundColor = [UIColor blueColor];
            break;

        default:
            break;
    }
    
}



@end
