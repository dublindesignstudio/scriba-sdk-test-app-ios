//
//  UITextView+HTML.m
//  scribaSDKTest
//
//  Created by lei_zhang on 4/25/16.
//  Copyright © 2016 Scriba. All rights reserved.
//

#import "UITextView+HTML.h"

@implementation UITextView (HTML)

- (void)setHTMLFromString:(NSString *)string {
    
    string = [string stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%fpx;}</style>",
                                              self.font.fontName,
                                              self.font.pointSize]];
    self.attributedText = [[NSAttributedString alloc] initWithData:[string dataUsingEncoding:NSUnicodeStringEncoding]
                                                           options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                     NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                documentAttributes:nil
                                                             error:nil];
}



@end
