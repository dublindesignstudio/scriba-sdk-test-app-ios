//
//  BasicViewController.m
//  scribaSDKTest
//
//  Created by lei_zhang on 4/17/16.
//  Copyright © 2016 Scriba. All rights reserved.
//

#import "BasicViewController.h"


@implementation BasicViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [ScribaStylusManager sharedManager].delegate = self;
}

@end
