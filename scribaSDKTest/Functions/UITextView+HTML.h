//
//  UITextView+HTML.h
//  scribaSDKTest
//
//  Created by lei_zhang on 4/25/16.
//  Copyright © 2016 Scriba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (HTML)

- (void)setHTMLFromString:(NSString *)string;

@end
