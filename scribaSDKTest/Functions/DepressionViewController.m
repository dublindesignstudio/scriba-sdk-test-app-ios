//
//  DepressionViewController.m
//  scribaSDKTest
//
//  Created by lei_zhang on 4/17/16.
//  Copyright © 2016 Scriba. All rights reserved.
//

#import "DepressionViewController.h"


@interface DepressionViewController()

@property (nonatomic, assign) IBOutlet UILabel *infoLabel;
@property (nonatomic, assign) IBOutlet UILabel *hapticsLabel;

@end

@implementation DepressionViewController

/*!
 * @discussion Callback method when depression value is changed
 * @param device The scriba device triggers the event
 * @param depression The depression value passes from scriba device
 */
-(void)didChangedDepressionForDevice:(ScribaStylusDevice*)device depression:(float)depression{
    
    //[ScribaStylusManager sharedManager].currentDeviceButtonDepression
    
    self.infoLabel.text = [NSString stringWithFormat:@"data %.2f",depression];
}



@end
