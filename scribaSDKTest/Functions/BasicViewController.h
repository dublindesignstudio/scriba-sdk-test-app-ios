//
//  BasicViewController.h
//  scribaSDKTest
//
//  Created by lei_zhang on 4/17/16.
//  Copyright © 2016 Scriba. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <ScribaSDK/ScribaStylusManager.h>


@interface BasicViewController : UIViewController <ScribaStylusManagerDelegate>

@end
