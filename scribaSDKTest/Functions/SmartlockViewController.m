//
//  SmartlockViewController.m
//  scribaSDKTest
//
//  Created by lei_zhang on 4/19/16.
//  Copyright © 2016 Scriba. All rights reserved.
//

#import "SmartlockViewController.h"


//static NSString *introductionString = @"Tap lock icon to enable the smart lock";
static NSString *enableString = @"Smart-Lock will engage if Scriba detects that it is being held at constant pressure. Press the Icon to deactivate Smart-Lock detection.";
static NSString *activeString = @"Smart-Lock is Engaged. A single click will deactivate Smart-Lock";
static NSString *deactiveString = @"Smart-Lock has been deactivated";


static NSString *lockStatusEngaged = @"Smart-Lock Engaged";
static NSString *lockStatusDeactivated = @"Smart-Lock Deactivated";
static NSString *lockStatusEnabled = @"Smart-Lock Enabled";


@interface SmartlockViewController()

@property (nonatomic , weak) IBOutlet UILabel *introductionLabel;
@property (nonatomic , weak) IBOutlet UILabel *smartlockValueLabel;
@property (nonatomic , weak) IBOutlet UILabel *smartlockIconLabel;


@property (nonatomic , weak) IBOutlet UIImageView *lockImageView;
@property (nonatomic) BOOL isLocked;

//@property (nonatomic) BOOL smartLockTriggered;

@end

@implementation SmartlockViewController
{
    UITapGestureRecognizer *tapGesture;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSmartLock)];
    
    [self.lockImageView addGestureRecognizer:tapGesture];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(smartLockChanged:) name:WDLockChangedNotification object:nil];
    
    //set default
    NSString *imageName;
    
    if ([ScribaStylusManager sharedManager].isSmartLockEnabled)
    {
        imageName = @"sizeLock_inactive";
        self.introductionLabel.text = enableString;
        self.smartlockIconLabel.text = lockStatusEnabled;
    }
    else
    {
        imageName = @"sizeLock_default";
        self.introductionLabel.text = deactiveString;
        self.smartlockIconLabel.text = lockStatusDeactivated;

    }
 
    self.lockImageView.image = [UIImage imageNamed:imageName];
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:WDLockChangedNotification object:nil];
    
    [self.lockImageView removeGestureRecognizer:tapGesture];
    
    [[ScribaStylusManager sharedManager] removeSmartLock];
}

#pragma mark - private methods
- (void)tapSmartLock
{
    NSString *imageName = @"";
    NSString *lockMsg = @"";
    BOOL isSmartLockEnabled = [ScribaStylusManager sharedManager].isSmartLockEnabled;
    if (isSmartLockEnabled) {
        [[ScribaStylusManager sharedManager] enableSmartLock:NO];
        imageName = @"sizeLock_default";
        self.introductionLabel.text = deactiveString;
        lockMsg = lockStatusDeactivated;
    }
    else{
        
        if (self.isLocked)
        {
            imageName = @"sizeLockOn";
            self.introductionLabel.text = activeString;
            lockMsg = lockStatusEngaged;
        }
        else
        {
            imageName = @"sizeLock_inactive";
            self.introductionLabel.text = enableString;
            self.smartlockIconLabel.text = @"Smart-Lock Enabled";
            lockMsg = lockStatusEnabled;
        }
        
        [[ScribaStylusManager sharedManager] enableSmartLock:YES];
    }
    
    self.lockImageView.image = [UIImage imageNamed:imageName];
    self.smartlockIconLabel.text = lockMsg;
}

-(void)didChangedDepressionForDevice:(ScribaStylusDevice*)device depression:(float)depression{

    if (!self.isLocked || ![ScribaStylusManager sharedManager].isSmartLockEnabled) {
        self.smartlockValueLabel.text = [NSString stringWithFormat:@"%.2f",depression];
    }
    
}

- (void)smartLockChanged:(NSNotification*)notification
{
    NSString *lockMsg = @"";
    self.isLocked = [notification.object boolValue];
    if(self.isLocked){
        
        //send a buzz to scriba
        [[NSNotificationCenter defaultCenter] postNotificationName:BuzzNotification object:nil];
        
        //change icon
        self.lockImageView.image = [UIImage imageNamed:@"sizeLockOn"];
        self.introductionLabel.text = activeString;
        lockMsg = lockStatusEngaged;
    } else {
        self.lockImageView.image = [UIImage imageNamed:@"sizeLock_inactive"];
        self.introductionLabel.text = enableString;
        lockMsg = lockStatusEnabled;
    }
    
    self.smartlockIconLabel.text = lockMsg;

}



@end
