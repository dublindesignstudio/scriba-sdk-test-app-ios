//
//  ClickViewController.m
//  scribaSDKTest
//
//  Created by lei_zhang on 4/17/16.
//  Copyright © 2016 Scriba. All rights reserved.
//

#import "ClickViewController.h"

@interface ClickViewController() 

@property (nonatomic, assign) IBOutlet UITextView *descTextView;

@property (nonatomic, assign) IBOutlet UILabel *infoLabel;

@property (nonatomic, assign) IBOutlet UILabel *oneClickLabel;
@property (nonatomic, assign) IBOutlet UILabel *doubleClickLabel;
@property (nonatomic, assign) IBOutlet UILabel *tripleClickLabel;


- (IBAction)buzz:(id)sender;
- (IBAction)doubleBuzz:(id)sender;

@end

@implementation ClickViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.descTextView.text = NSLocalizedString(@"click.desc.text", nil);
    
    [[ScribaStylusManager sharedManager] disableHapticsBuzz];
}


/*!
 * @discussion Callback method when single click event is triggered
 * @param device The scriba device triggers the event
 */
-(void)didSingleClickWithDevice:(ScribaStylusDevice*)device{
    
    self.infoLabel.text = @"Single Click";
    [self highlightClicks:0];
}

/*!
 * @discussion Callback method when double clicks event is triggered
 * @param device The scriba device triggers the event
 */
-(void)didDoubleClickWithDevice:(ScribaStylusDevice*)device{
    
    self.infoLabel.text = @"Double Click";
    [self highlightClicks:1];
}

/*!
 * @discussion Callback method when trible clicks event is triggered
 * @param device The scriba device triggers the event
 */
-(void)didTrippleClickWithDevice:(ScribaStylusDevice*)device{
    
    self.infoLabel.text = @"Triple Click";
    [self highlightClicks:2];
}

- (IBAction)buzz:(id)sender{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:BuzzNotification object:nil];
}

- (IBAction)doubleBuzz:(id)sender
{
     [[NSNotificationCenter defaultCenter] postNotificationName:BuzzNotification object:[NSNumber numberWithInt:2]];
}


- (void)highlightClicks:(NSUInteger)clicks{
    
    self.oneClickLabel.backgroundColor = [UIColor lightGrayColor];
    self.doubleClickLabel.backgroundColor = [UIColor lightGrayColor];
    self.tripleClickLabel.backgroundColor = [UIColor lightGrayColor];
    
    switch (clicks) {
        case 0:
            self.oneClickLabel.backgroundColor = [UIColor blueColor];
            break;
        case 1:
            self.doubleClickLabel.backgroundColor = [UIColor blueColor];
            break;
        case 2:
            self.tripleClickLabel.backgroundColor = [UIColor blueColor];
            break;
            
        default:
            break;
    }
    
}

@end
