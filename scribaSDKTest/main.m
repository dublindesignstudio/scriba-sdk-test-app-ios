//
//  main.m
//  scribaSDKTest
//
//  Created by Brian Egan on 24/08/2015.
//  Copyright (c) 2015 Scriba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
