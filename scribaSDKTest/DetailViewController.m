//
//  DetailViewController.m
//  scribaSDKTest
//
//  Created by lei_zhang on 4/17/16.
//  Copyright © 2016 Scriba. All rights reserved.
//

#import "DetailViewController.h"
#import <ScribaSDK/ScribaStylusManager.h>

@interface DetailViewController()


@property (nonatomic, assign) IBOutlet UILabel *infoLabel;



- (IBAction)clickDemo:(id)sender;
- (IBAction)depressionDemo:(id)sender;
//- (IBAction)BuzzDemo:(id)sender;
//- (IBAction)HapticsDemo:(id)sender;
- (IBAction)smartlockDemo:(id)sender;


@end


@implementation DetailViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self updateLabel];
    
    [[ScribaStylusManager sharedManager] disableHapticsBuzz];
}


-(void)updateLabel
{
    NSString *info;
    
    if([ScribaStylusManager sharedManager].connectedDevice){
        info = [NSString stringWithFormat:@"%@: \nBattery: %.1f%%",
                [ScribaStylusManager sharedManager].connectedDevice.name,
                [ScribaStylusManager sharedManager].currentDeviceBatteryState*100.0];
        
    } else {
        info = @"No device connected";
    }
    
    self.infoLabel.text = info;
}


- (IBAction)clickDemo:(id)sender{
    
    [self performSegueWithIdentifier:@"clicks" sender:nil];
}

- (IBAction)depressionDemo:(id)sender{
    
    [self performSegueWithIdentifier:@"haptics" sender:nil];
}

- (IBAction)smartlockDemo:(id)sender{

    [self performSegueWithIdentifier:@"smartlock" sender:nil];
    
}


@end
