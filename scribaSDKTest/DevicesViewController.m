//
//  ViewController.m
//  ScribaSDKTest
//
//  Created by Pawel Sikora on 28/06/15.
//  Copyright (c) 2015 Dublin Design Studio. All rights reserved.
//

#import "DevicesViewController.h"
#import <ScribaSDK/ScribaStylusManager.h>
#import "SWTableViewCell.h"

@interface DevicesViewController () <UITableViewDataSource, UITableViewDelegate, SWTableViewCellDelegate,ScribaStylusManagerDelegate>

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, assign) IBOutlet UITableView *tableView;

@property (nonatomic) NSMutableArray *recognized;
@property (nonatomic) NSMutableArray *unknownDevices;

@end

@implementation DevicesViewController
    

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.titleLabel.text = NSLocalizedString(@"introduction.title.text", nil);
    
    [ScribaStylusManager sharedManager].delegate = self;
    [[ScribaStylusManager sharedManager] startScanning];
    [[ScribaStylusManager sharedManager] disableHapticsBuzz];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:self selector:@selector(stylusConnected:)
               name:WDStylusDidConnectNotification object:nil];
    [nc addObserver:self selector:@selector(stylusDisconnected:)
               name:WDStylusDidDisconnectNotification object:nil];
    [nc addObserver:self selector:@selector(stylusListUpdated:)
               name:WDStylusListUpdatedNotification object:nil];
    
    self.recognized = [[NSMutableArray alloc] init];
    self.unknownDevices = [[NSMutableArray alloc] init];

    [self refeshScribaDevices];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(blueToothStatusChanged:) name:WDBlueToothStateChangedNotification object:nil];
}

- (void) dealloc
{
    [[ScribaStylusManager sharedManager] stopScanning];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self setBluetoothImage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private methods
- (void)blueToothStatusChanged:(NSNotification*)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setBluetoothImage];
    });
}

- (void)setBluetoothImage
{
    if ([ScribaStylusManager sharedManager].isBlueToothEnabled)
    {
        self.navigationItem.rightBarButtonItem = nil;
    }
    else
    {
        UIImage *image = [UIImage imageNamed:@"bluetoothOff.png"];
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
        [btn setImage:image forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(openBluetoothInSettings:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:btn];

        self.navigationItem.rightBarButtonItem = rightItem;
    }
}

- (void)openBluetoothInSettings:(id)sender
{
    //to setting screen
    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)refeshScribaDevices
{
    [self.recognized removeAllObjects];
    [self.unknownDevices removeAllObjects];
    
    NSArray *devices = [[ScribaStylusManager sharedManager] getListOfScribaDevices];

    for (ScribaStylusDevice *device in devices) {
        
        if ([device isRecognizedDevice]) {
            [self.recognized addObject:device];
        }
        else{
            [self.unknownDevices addObject:device];
        }
    }
}

- (void) stylusConnected:(NSNotification *)aNotification
{
    [self refeshScribaDevices];
    [self.tableView reloadData];
}

- (void) stylusDisconnected:(NSNotification *)aNotification
{
    [self refeshScribaDevices];
    [self.tableView reloadData];
}

- (void) stylusListUpdated:(NSNotification *)aNotification
{
    [self refeshScribaDevices];
    [self.tableView reloadData];
}

- (NSArray*)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor lightGrayColor]
                                                title:@"Forget"];
    
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                title:@"Buzz"];
    return rightUtilityButtons;
}

#pragma mark - table view stuff

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger retVal = 0;
    if (section == 0) {
        retVal = (self.recognized.count > 0) ? self.recognized.count : self.unknownDevices.count;
    }
    else{
        retVal = self.unknownDevices.count;
    }
    
    return retVal;

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger stylusSeciton = 0;
    
    if (self.recognized.count > 0) {
        stylusSeciton += 1;
    }
    
    if (self.unknownDevices.count > 0) {
        stylusSeciton += 1;
    }
    
    return stylusSeciton;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *newDevicesTitle = @"New Devices";
    NSString *recognizedTitle = @"Recognized Device";
    
    if(section == 0)
    {
        if (self.recognized.count == 0) {
            return newDevicesTitle;
        }
        
        return recognizedTitle;
    }
    else
    {
        return newDevicesTitle;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentyfier = @"Cell";
    
    SWTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentyfier];
    
    if (cell == nil) {
        cell = [[SWTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentyfier];
        cell.delegate = self;
    }
    
    ScribaStylusDevice *device = nil;
    if (indexPath.section == 0) {
        device = (self.recognized.count > 0) ? self.recognized[indexPath.row] : self.unknownDevices[indexPath.row];
    }
    else{
        device = self.unknownDevices[indexPath.row];
    }
    
    if(device.state == ScribaDeviceStateConnected){
        cell.rightUtilityButtons = [self rightButtons];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.rightUtilityButtons = nil;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.textLabel.text = device.name;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ScribaStylusDevice *device = nil;
    if (indexPath.section == 0) {
        device = (self.recognized.count > 0) ? self.recognized[indexPath.row] : self.unknownDevices[indexPath.row];
    }
    else{
        device = self.unknownDevices[indexPath.row];
    }
    
    if(device.state == ScribaDeviceStateConnected){
        //go to detail screen
        [self performSegueWithIdentifier:@"details" sender:nil];
    }
    else{
        [[ScribaStylusManager sharedManager] tryConnectDevice:device completion:^(NSError *error) {
            //[self performSegueWithIdentifier:@"details" sender:nil];
        }];
    }
    
}

#pragma mark - SWTableViewDelegate
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    switch (index)
    {
        case 0:
        {
            // Delete button was pressed
            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];

            if (indexPath.section == 0)
            {
                ScribaStylusDevice *device = self.recognized[indexPath.row];
                if (device.state == ScribaDeviceStateConnected)
                {
                    [[ScribaStylusManager sharedManager] disconnectCurrentDeviceCompletion:^(NSError *error) {
                        [device setRecognizedDevice:NO];
                    }];

                }

                [self.tableView reloadData];
            }
            break;
        }
        case 1:
        {
            // Delete button was pressed
            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];

            if (indexPath.section == 0)
            {
                ScribaStylusDevice *device = self.recognized[indexPath.row];
                if (device.state == ScribaDeviceStateConnected)
                {
                    //three times buzz
                    for (int i=0; i<5; i++)
                    {
                        [[NSNotificationCenter defaultCenter] postNotificationName:BuzzNotification object:nil];
                    }
                }
            }
            break;
        }
        default:
            break;
    }
}


#pragma mark - scriba stylus manager

-(void)didFoundDevices:(NSArray *)devices{
    NSLog(@"did found devices: %lu",(unsigned long)devices.count);
    
    [self postNotificationOnMainQueue:WDStylusListUpdatedNotification userInfo:nil];
}

-(void)didConnectedDevice:(ScribaStylusDevice *)device manager:(CBCentralManager *)manager{
    NSLog(@"did connected");
    [device setRecognizedDevice:YES];
    
    [self postNotificationOnMainQueue:WDStylusDidConnectNotification userInfo:nil];
}

-(void)didDisconnectDevice:(ScribaStylusDevice *)device{
    NSLog(@"did disconnected");

    [self postNotificationOnMainQueue:WDStylusDidDisconnectNotification userInfo:nil];
}

- (void) postNotificationOnMainQueue:(NSString *)name userInfo:(NSDictionary *)userInfo
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:name object:self userInfo:userInfo];
    });
}



@end
