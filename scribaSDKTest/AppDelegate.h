//
//  AppDelegate.h
//  ScribaSDKTest
//
//  Created by Pawel Sikora on 28/06/15.
//  Copyright (c) 2015 Dublin Design Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XOSplashVideoController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,XOSplashVideoDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIViewController *startViewController;

@end

